// console.log("Hello World");

// [Section] Syntax, Statements and comments
// Statements in programming are instructions that we tell the computer to perform
// Js statements usually end with semicolon(;).
// Semicolons are not required in JS, but we will use it to help us train to locate where a statements end.
// A syntax in programming, it is the set of rules that describes how statements must be constructed.
// All lines/blocks of code should be written in specific manner to work. this is due to how these codes where initially programmed to function in certain manner.

	// Comments are parts of the code that gets ignored by the language
	// Comments are meant to describe the written code.

/*
	There are two types of comments:
	1. The single-line comment denoted by two slashes. crtl + forwardslash
	2. The multi-line comment denoted by slash and asterisk. crtl + shift + forwardslash
*/

// [Section] Variables

// Variables are use to contain data.
// Any information that is used by an application is stored in what we call the "memory".
// When we create variables, certain portions of a device's memory is given a name that we call variables.
// This makes it easier for us to associate information stored in our devices to actual "names about information"

// Declaring variables
// Declaring variables - it tells our devices that a variable name is created and is ready to store data.
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined
	// Syntax
		//let / const variablename;
let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome browser's console. 
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code.
console.log(myVariable);

/*
	Guides in writing variables:
		1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value
		2. Variable names should start with lowercase character, use camelCase for multiple words.
		3. For constant variables, use the 'const' keyword.
			using let: we can change the value of the variable.
			using const: we cannot change the value of the variable.
		4. Variable name should be indicative (descriptive) of the value being stored to avoid confusion.	
*/

// Declare and initialize variables
// Initializing variables - the instance when a variable is given it's initial / starting value
	// Syntax
		// let/const variableName = value;
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);


// Reassigning variable value
	// syntax
		// variableName = newValue;

productPrice = 15000;
console.log(productPrice);

	// let variable cannot be re-declared within its scope

// Values of constants cannot be changed and will simply return an error.
// interest = 4.489;
// console.log(interest);

// Reassigning variables and initializing variables

let supplier;
// Initialization
supplier = "Zuitt Store";
// Reassignment
supplier = "zuitt Merch";

const name = "John Bazil Valdez";
// name = "John Bazil Valdez"

a = 5;
console.log(a);
var a;

// b = 6;
// console.log(b);
// let b;

	// let/const local/globaly scope
	// Scope essentially means where these variables are available for use
	// let and const are block scoped.
	// A block is a chunk of code bounded by {}. A block is curly braces. Anything within the curly braces is a block.

	let outerVariable = "Hello from the other side";

	// {
	// 	let innerVariable = "Hello from the block"
	// 	console.log(innerVariable);
	// }

	{
		let innerVariable = "Hello from the block"
		console.log(innerVariable);
		let outerVariable = "Hello from the second block";
		// console.log(outerVariable);
	}


	console.log(outerVariable);
	// console.log(innerVariable);

// Multiple variable declarations
// Multiple variable can be declared in one line.
let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// Using a variable with a reserved keyword

// const let = "hello";
// console.log(let);

//[Sections] Data types
// String - are series of characters that create a word, a phrase, a sentence or anything related to creating text.
// String in JavaScript can be written using either single (') and double (") quote.
// In other programming languages, only the double can be used for creating strings.

let country = "Philippines";
let province = 'Metro Manila';

// Concatenate
// Multiple string values can be c ombined to create a single string using the "+" symbol.

let fullAddress = province +", "+ country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape characters (\) in string in combination with other characters can produce different effects.

// "\n" refers to creating a new line between text
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early."

message = 'John\'s employees went home early.';

console.log(message);

// Numbers
// Integers/Whole numbers
let count = "26";
let headCount = 26;
console.log(count);
console.log(headCount);

// Decimal Numbers / Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios.

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

console.log("isMarried:" + isMarried);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values.
// Arrays can store different data types but is normally use to stode similar data types.
	
	// Similar data types
		// Syntax
		// let/const arrayName = [elementA, elementB, elementC . . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(grades[1]);

// Different data types
// Storing different data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true]
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// they're used to create a complex data that contains pieces of information that are relevant to each other.

// Syntax:
	// let/const objectName = {
		// propertyA: value,
		// propertyB: value,
		// }

let person = {
	fullName: "John Dela Cruz",
	age: 35,
	isMarried: false,
	contact:["09171234567", "8123 4567"],
	address:{
		houseNumber: '345',
		city: "manila"
	}
};

console.log(person);
console.log(person.contact[1]);
console.log(person.address.city);

// typeof operator is used to determine the type of data or the value of a variable.

console.log(typeof person);
// Array is a special type of object with methods and functions to manipulate it.
console.log(typeof details);

	/*
		Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */
    const anime = ["one piece", "one punch man", "attack on titans"];
    console.log(anime);
    anime[0] = "kimetsu no yaiba";
    console.log(anime);

// Null
// It is used to intentionally expressed the absence of a value in a variable/ initialization.
// Null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified.
let spouse = null;
console.log(spouse);
// Undefined
// represents the state of a variable that has been declared but wtihout value
let fullName;
console.log(fullName);
